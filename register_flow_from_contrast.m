% This script fits a line across a contrast map for each scan (registered 
% with each other) and looks at the contrast values at each position on the
% line to see the variation between scans and the sensitivity of detecting
% vessels vs the background

clear
clc
close all
%% Initialise (0.5 ms to 64 ms)
scan1 = load("contrast_ID0Scan1_noreg.mat").new_mat_maps;   %%
scan2 = load("contrast_ID0Scan2_noreg.mat").new_mat_maps;   %%
scan3 = load("contrast_ID0Scan3_noreg.mat").new_mat_maps;   %%
scan4 = load("contrast_ID0Scan4_noreg.mat").new_mat_maps;   %%
scan5 = load("contrast_ID0Scan5_noreg.mat").new_mat_maps;   %%
scan6 = load("contrast_ID0Scan1D2_noreg.mat").new_mat_maps; %%
scan7 = load("contrast_ID0Scan2D2_noreg.mat").new_mat_maps; %%
scan8 = load("contrast_ID0Scan3D2_noreg.mat").new_mat_maps; %%
scan9 = load("contrast_ID0Scan4D2_noreg.mat").new_mat_maps; %%
scan10 = load("contrast_ID0Scan5D2_noreg.mat").new_mat_maps; %%
filename = 'D:\Speckle\flow_contrast\contrast_ID0.gif';           %%
exp_times = [0.5, 1, 2, 4, 8, 16, 32, 64];
reg_scans = zeros(2152,2552,10);


% Configure registration
[optimizer, metric] = imregconfig('multimodal'); % Change to monomodal since the sensor is the same
optimizer.MaximumIterations = 400; % Increase this to increase quality but decrease performance
default = 3.90625e-4; % Return initial radius to normal value
optimizer.InitialRadius = default;

% Initialise transformation matrix
tform = affine2d();
tform_array = repmat(tform, [1 1 10]); % Could just change this to [numel(D)] in future
tforms = zeros(3,3,10);

all_scans = cat(3,scan1,scan2,scan3,scan4,scan5,scan6,scan7,scan8,scan9,scan10);

%% Align all scans - registration should use the same tform for the same scan
for iterations = 1:5 % Iterate 3 times

    if iterations == 1
        % Use 16ms scans as these seem to have the best image quality
        combined_scans = cat(3,scan1(:,:,6),scan2(:,:,6),scan3(:,:,6),scan4(:,:,6),scan5(:,:,6),scan6(:,:,6),scan7(:,:,6),scan8(:,:,6),scan9(:,:,6),scan10(:,:,6));
        fixed = mean(combined_scans,3); % average image
        fixed = imresize(fixed,0.3);
        r_and_c = size(fixed);
        small_cont_maps = zeros(r_and_c(1),r_and_c(2),10);
        moving_array = zeros(r_and_c(1),r_and_c(2),10);
    else
        fixed = mean(moving_array,3);
    end

    fixed = double(adapthisteq(uint8(fixed/max(max(fixed))*255)));
    %figure,imshow(fixed,[])

    parfor k = 1:10 % For each of the 10 scans

        new_optimizer = optimizer;

        if iterations == 1 % Make the contrast maps the same size as the fixed image
            moving = combined_scans(:,:,k);
            moving = imresize(moving,0.3);
            small_cont_maps(:,:,k) = moving;
        end

        % Perform the registration
        tform = tform_array(:,:,k);
        tform = imregtform(small_cont_maps(:,:,k), fixed, 'affine', optimizer, metric,'InitialTransformation',tform);

        tform_array(:,:,k) = tform; % Write new tform into tform array  
    end

    for j = 1:10
        if iterations < 3
            moving_array(:,:,j) = imwarp(small_cont_maps(:,:,j),tform_array(:,:,j),'OutputView',imref2d(size(fixed)));
%             moving_array(:,:,k) = imwarp(small_cont_maps(:,:,k),tform,'OutputView',imref2d(size(fixed)));
        end    
    end
end
count = 1;
% Apply the same transform for all images in the same scan (2ms to 64 ms)
for scan = 1:10
    tform = tform_array(:,:,scan);
    scaled_tform = tform;
    scaled_tform.T(3,1:2) = tform.T(3,1:2)*2152/646;
    for num_exp = 3:length(exp_times)
        j = (scan-1)*8+num_exp;
        reg_scans(:,:,count) = imwarp(all_scans(:,:,j),scaled_tform,'OutputView',imref2d(size(all_scans(:,:,j))));
        count = count + 1;
    end
end


%% Create a gif (to check registration)
for idx = 1:60
    image = reg_scans(:,:,idx);

    % convert image into uint8 (as gif only accepts uint8)
    image_u8 = uint8(image*255/max(max(image)));
    image_u8 = imresize(image_u8,0.3);

    % write the gif
    if idx == 1
        imwrite(image_u8,filename,'gif','LoopCount',Inf,'DelayTime',0.1);
    else
        imwrite(image_u8,filename,'gif','WriteMode','append','DelayTime',0.1);
    end
end

%% Make a flow map for all 5 scans (2ms to 32 ms)
% ft = fittype('modelLiu(x, t_c, beta, c, p)','coefficients',{'t_c','beta','c','p'});
ft = fittype('modelKirkGauss(x, t_c, beta, c)','coefficients',{'t_c','beta','c'});
T_2 = [2.0, 4.0, 8.0, 16.0, 32.0]; 

% 1-5, 7-12, 13-17, 19-23, 25-29 as the registered contrast maps above go
% from 2 ms to 64 ms but the flow maps have exposure times 2 ms to 32 ms
index = [1,7,13,19,25,31,37,43,49,55];
for u = index
    norm_K = reg_scans(:,:,u:u+4);

   if u == 1
       % crop one to get rect
        I = norm_K(:,:,4);
        I = imresize(I,0.1);
        % display screen to crop image
        [~, rect] = imcrop(I);
    end

    avg_cont = zeros(ceil(rect(4)),ceil(rect(3)),5);  % initialise how many contrast maps stored

    for i = 1:5
        I = norm_K(:,:,i) ;   % Read image 
        I = imresize(I,0.1);
        avg_cont(:,:,i) = imcrop(I,rect) ;        % crop image 
    end

%     imshow(imresize(norm_K(:,:,4),0.1))
% 
%     hold on;
%     rectangle('Position',rect,'EdgeColor','r')

    % for j = 1 : 5
    %     avg_cont(:,:,j) = first_bg_subtract(avg_cont(:,:,j));
    % end

    [r,c,~] = size(avg_cont);
    tc_vals = zeros(r,c);
    rsq_vals = zeros(r,c);
    x = T_2';

    for row = 1:r
        parfor col = 1:c
            p = zeros(1,5);
            % y column size 5 rows 1 column (same as x)
            p(1,1:5) = avg_cont(row,col,1:5);
            p = p';
            [f,gof] = fit(x,p,ft,'Lower', [0 1 0], 'Upper', [50 1 0], 'StartPoint', [0.2 1 0], 'MaxIter', 5000);
            tc_vals(row,col) = f.t_c;
            rsq_vals(row,col) = gof.rsquare;
            coeffvalues(f);
        end
    end

    flow_map = 1./tc_vals;

    
%     figure,imshow(rsq_vals,[])
%     colormap(jet)
%     colorbar
%     caxis([0 1])
    
    %load 'ID0Scan2forShow.mat'
    figure,imshow(flow_map,[])
    colormap(jet)
    colorbar
    caxis([0 30])

    % final = first_bg_subtract(flow_map);
    % figure,imshow(final,[])
    % colormap(jet)
    % colorbar
    % caxis([0 4])
    
    position = find(index == u);
    
    fname = 'ID0Scan' + string(position) + 'unreg.mat';                          %%%%%%%%%%%%%%%%%%%%

    save(fname, 'flow_map')     
end


%% Register the flow maps

flow_map1 = load('ID0Scan1unreg.mat').flow_map;                         %%%%%%%%%%%%
flow_map2 = load('ID0Scan2unreg.mat').flow_map;                         %%%%%%%%%%%%
flow_map3 = load('ID0Scan3unreg.mat').flow_map;                         %%%%%%%%%%%%
flow_map4 = load('ID0Scan4unreg.mat').flow_map;                         %%%%%%%%%%%%
flow_map5 = load('ID0Scan5unreg.mat').flow_map;                         %%%%%%%%%%%%
flow_map6 = load('ID0Scan6unreg.mat').flow_map;                         %%%%%%%%%%%%
flow_map7 = load('ID0Scan7unreg.mat').flow_map;                         %%%%%%%%%%%%
flow_map8 = load('ID0Scan8unreg.mat').flow_map;                         %%%%%%%%%%%%
flow_map9 = load('ID0Scan9unreg.mat').flow_map;                         %%%%%%%%%%%%
flow_map10 = load('ID0Scan10unreg.mat').flow_map;                       %%%%%%%%%%%%

%flow_map5(215:216,:) = 0;                               

all_scans = cat(3,flow_map1,flow_map2,flow_map3,flow_map4,flow_map5,flow_map6,flow_map7,flow_map8,flow_map9,flow_map10);


norm_K = all_scans(:,:,1);
norm_K = uint8(norm_K*255/35);
[~, rect] = imcrop(norm_K);

new_allScans = zeros(ceil(rect(4)),ceil(rect(3)),10);  % initialise how many contrast maps stored

for i = 1:10
    I = all_scans(:,:,i) ;   % Read image 
    new_allScans(:,:,i) = imcrop(I,rect) ;        % crop image 
end


gifname = 'D:\Speckle\flow_contrast\flow_ID0.gif';                   %%%%%%%%%%%%

% Configure registration
[optimizer, metric] = imregconfig('multimodal'); % Change to monomodal since the sensor is the same
optimizer.MaximumIterations = 400; % Increase this to increase quality but decrease performance
default = 5e-4; % Return initial radius to normal value
optimizer.InitialRadius = default;

%Initialise transformation matrix
tform = affine2d();
tform_array = repmat(tform, [1 1 10]); % Could just change this to [numel(D)] in future



% Align all scans - registration should use the same tform for the same scan
for iterations = 1:2 % Iterate 3 times

    if iterations == 1
        
        fixed = mean(new_allScans,3); % average image
        r_and_c = size(fixed);
        small_cont_maps = zeros(r_and_c(1),r_and_c(2),10);
        moving_array = zeros(r_and_c(1),r_and_c(2),10);
    else
        fixed = mean(moving_array,3);
    end
    
    fixed = uint8(fixed*255/60);
    
    %fixed = imadjust(fixed);
    fixed = adapthisteq(fixed);
%     fixed = adapthisteq(fixed);
%     fixed = adapthisteq(fixed);
%     fixed = adapthisteq(fixed);
%     
    
    
    
    
     figure,imshow(fixed)

    parfor k = 1:10 % For each of the 10 scans

        new_optimizer = optimizer;
        movingImage = uint8(new_allScans(:,:,k)*255/60);
       

        % Perform the registration
        moving_array(:,:,k) = imregister(movingImage, fixed, 'affine', optimizer, metric);
        moving_array(:,:,k) = double(moving_array(:,:,k)*60/255);
    end

   
end


% Create a gif (to check registration)
for idx = 1:10
    flow_map = moving_array(:,:,idx);

    %convert image into uint8 (as gif only accepts uint8)
    imagetostore = uint8(flow_map*255/60);
   

    % write the gif
    if idx == 1
        imwrite(imagetostore,gifname,'gif','LoopCount',Inf,'DelayTime',0.1);
    else
        imwrite(imagetostore,gifname,'gif','WriteMode','append','DelayTime',0.1);
    end
       
    fname = 'ID0Scan' + string(idx) + 'Reg.mat';                   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    save(fname, 'flow_map')
end
