% This script creates contrast maps from raw speckle images and stores
% values as doubles as part of a 3D array. Then registration is performed
% on the contrast maps before they are saved as averages.

% can comment out to just run sections of the script
clear
clc

%%
% Contrast map Generation: Creating contrast map double from raw speckle 
% images as well as contrast maps that will be preprocessed. 

X = 'D:\Speckle\ID0Scan2\2021-05-18_09-45-03_Sequence_0_02';               %%
Direct = dir(fullfile(X,'*ms.pgm'));

noSNRfolder = 'D:\Speckle\ID0Scan2\no_SNR';                                %%
mkdir(noSNRfolder);

exposures = [0.5,1.0,2.0,4.0,8.0,16.0,32.0,64.0];                            
kernel = ones(9);
contrast_map_array = zeros(2152,2552,50);    % will increase if more images  
% set position of contrast maps within the array
pos = 1;

for h = exposures
    file_end3 = sprintf('*%04.1fms.pgm',h);
    next = dir(fullfile(X,file_end3));
    
    for n = 1:numel(next)
        image = imread(fullfile(X, next(n).name));
        contrast4py = BiasCalibrate(image);
        contrast4py = speckleContrastCallaghan(double(contrast4py), kernel);
        contrast4Pre = speckleContrastCallaghan(double(image), kernel);  
        
        % store contrast map into an array for future input to python
        contrast_map_array(:,:,pos) = contrast4py;  
        pos = pos + 1;
        
        % form contrast maps that will be preprocessed and store to a folder
        contrast_map_preprocess = uint8(contrast4Pre*255/max(max(contrast4Pre)));
        [~,name,ext] = fileparts(next(n).name);
        F = fullfile(noSNRfolder,strcat(name,ext));
        imwrite(contrast_map_preprocess,F);
    end
end

%%
% Thresholding: This section preprocesses the no SNR contrast maps formed 
% above for future registration

% directory of all images saved under the noSNR folder made above
SNRdir = dir(fullfile(noSNRfolder,'*ms.pgm'));

% folder name for preprocessed contrast maps for registration
preprocess = 'D:\Speckle\ID0Scan2\preprocessed';                           %%
mkdir(preprocess);

parfor i = 1:numel(SNRdir)

    fixed = imread(fullfile(noSNRfolder,SNRdir(i).name));
    
    % masking the centre bright white spot
    M = mean(mean(fixed));
    [m,n] = size(fixed);
    
    for c = floor(0.46*n) : floor(0.59*n)
        for r = floor(0.35*n) : floor(0.49*n)
            if fixed(r,c) > uint8(1)
                fixed(r,c) = uint8(M);
            end
        end
    end
    
    % median filtering and enhancing contrast
    fixed = medfilt2(fixed,[3 3]);
    fixed = imadjust(fixed);
    fixed = adapthisteq(fixed);
    fixed = adapthisteq(fixed);
    fixed = adapthisteq(fixed);
    fixed = adapthisteq(fixed);
    fixed = adaptthresh(fixed, 0.3, 'NeighborhoodSize', [7,7], 'Statistic','median');
    M = mean(mean(fixed));
    fixed(fixed>uint8(M+50)) = uint8(255);
    
    % Close mask with disk
    radius = 3;
    decomposition = 8;
    se = strel('disk',radius,decomposition);
    fixed = imclose(fixed, se);
    
    % resize image for faster registration
    fixed = imresize(fixed, 0.3);
   
    % final median filtering for improved registration due to better 
    % intensity (not splotchy)
    fixed = medfilt2(fixed, [3 3]);
    
    

    % save preprocessed images to file
    [~,name,ext] = fileparts(SNRdir(i).name);
    F = fullfile(preprocess,strcat(name,ext));
    imwrite(fixed,F);

end

%%
% fixed image formation: initial registration of contrast maps to form an 
% averaged fixed image for proper registration. (affine, monomodal)

% directory of preprocessed images
preDirect = dir(fullfile(preprocess,'*ms.pgm'));

% Configure optimizer (monomodal: less images are successfully registered 
% but registration is better)
[optimizer, metric] = imregconfig('monomodal');
%optimizer.InitialRadius = 0.0004;

% form the fixed image by averaging contrast maps after only one
% registeration. May perform more to get a better fixed image    

I0 = imread(fullfile(preprocess,preDirect(1).name)); % Get initial average image
sumImage = double(I0); % Initialize to first image.

for i = 2:numel(preDirect) % Read in remaining images.
    nextImage = imread(fullfile(preprocess,preDirect(i).name));
    sumImage = sumImage + double(nextImage);
end

fixed = uint8(sumImage / numel(preDirect)); % first averaged image
imshow(fixed)

[r,c] = size(fixed);
% initial unregistered contrast maps here
moving_array = zeros(r,c,numel(preDirect)); 
% registered array: expect >30 images will register well
new_moving_array = zeros(r,c,30); 
% Array to be averaged for fixed: expect >10 images to pass translation limit
reduced_moving_array = zeros(r,c,10); 

tran_array = zeros(1,numel(preDirect));

parfor k = 1:numel(preDirect)

    moving = imread(fullfile(preprocess,preDirect(k).name));
    % Required for future if poor final fixed image results and another registration is needed
    moving_array(:,:,k) = moving; 

    % Perform the registration
    transform = imregtform(moving, fixed, 'affine', optimizer, metric);
    new_moving_array(:,:,k) = imwarp(moving,transform,'OutputView',imref2d(size(fixed)));
    
    tran_array(1,k) = transform.T(3,1);    

    % ViewImage = uint8(new_moving_array(:,:,k));
    % imshow(ViewImage)
end

count2 = 1;

for g = 1: length(tran_array)
    if abs(tran_array(g)) < 30
        reduced_moving_array(:,:,count2) = new_moving_array(:,:,g);
        count2 = count2 + 1;
    end      
end

finalfixed = uint8(mean(reduced_moving_array,3));

figure, imshow(finalfixed)

save('fixedID0Scan2.mat', 'finalfixed')                                           %%


%%
% Registration: registering preprocessed contrast maps then warping them to 
% each of the contrast_map_array, contrast maps and raw speckle images.
% (rigid, multimodal, initial radius = 0.0004)


% folder name for new registered speckle images 
reg_raw = 'D:\Speckle\ID0Scan2\registered_raw';                              %%
mkdir(reg_raw)

% folder name for new registered contrast maps
contrastfolder = 'D:\Speckle\ID0Scan2\registered_contrast';                   %%
mkdir(contrastfolder)

% create optimiser
[optimizer, metric] = imregconfig('multimodal');                             
optimizer.InitialRadius = 0.0004;

% initialise the array that will go into python
mat_maps = zeros(2152,2552,5);


% establish the average image as the reference
binary_fixed = finalfixed;
imshow(binary_fixed)

transform = affine2d();   

final_tran_array = cell(8,13);   % no of exp,no of scans

% make an array of transforms
for j = exposures
    
    % know which row to place value in
    position = find(exposures == j);
    
    % access directory of all images of a particular exposure
    file_end1 = sprintf('*%04.1fms.pgm',j);
    firstd = dir(fullfile(preprocess,file_end1));
    

    parfor v = 1:numel(firstd)
        % read the contrast map that is to be fixed to the reference 
        moving = imread(fullfile(preprocess,firstd(v).name)); 
        %imshow(moving)


        % register this image to the reference
        transform = imregtform(moving, binary_fixed, 'rigid', optimizer, metric);
        
        final_tran_array{position,v} = transform;
    end
end

% now warp transform to images

% start count of Direct access from 0
count = 1;
s_count = 1;  %here only if quantisation metric removed
% assume at least 50 images will pass the translation parameter, here only if quantisation metric removed
remove_cont_array = zeros(2152,2552,50);                                    

for e = exposures
    
    %remove_cont_array = zeros(2152,2552,8);
    
    % successful registration passing translation constraint count
    %s_count = 1     add if not passing through xcorr2
     
    % access directory of all images of a particular exposure
    file_end1 = sprintf('*%04.1fms.pgm',e);
    firstd = dir(fullfile(preprocess,file_end1));    
    file_end2 = sprintf('*%04.1fms.pgm',e);
    second = dir(fullfile(X,file_end2));
    
    % know which row to place value in
    posit = find(exposures == e);

    for b = 1:numel(firstd)
        % read the contrast map that is to be fixed to the reference 
        moving = imread(fullfile(preprocess,firstd(b).name)); 
        %imshow(moving)

        % read the corresponding raw speckle image that is to be warped
        % and scale it
        speckle_raw = imread(fullfile(X,second(b).name));
        speckle_raw = double(speckle_raw);
        speckle_raw = uint8(speckle_raw*255/max(max(speckle_raw)));


        % register this image to the reference
        final_transform = final_tran_array{posit,b};
        
        % give preprocessed contrast map, raw speckle and contrast map
        % (double) registration
        contrastRegistered = imwarp(moving,final_transform,'OutputView',imref2d(size(fixed)));
        
        final_transform.T(3,1:2) = final_transform.T(3,1:2)*1/0.3;
        movingRegistered = imwarp(speckle_raw,final_transform,'OutputView',imref2d(size(speckle_raw)));
        %viewImage = imadjust(movingRegistered);
        %imshow(movingRegistered)
        
        contrast_map_array(:,:,count) = imwarp(contrast_map_array(:,:,count), final_transform, 'OutputView',imref2d(size(contrast_map_array(:,:,count))));   
        
        % remove poorly registered frames with questionable translations
        if abs(final_transform.T(3,1)) < 70
            % Write this registered image to a newf (new folder)
            [~,name,ext] = fileparts(firstd(b).name);
            F = fullfile(reg_raw,strcat(name,ext));
            imwrite(movingRegistered,F);
                        
            C = fullfile(contrastfolder,strcat(name,ext));
            imwrite(contrastRegistered,C);
            
            remove_cont_array(:,:,s_count) = contrast_map_array(:,:,count);
            s_count = s_count + 1;        
        end
        
        count = count + 1;    
    end

    % averages all passed images of a single exposure
    %mat_maps(:,:,posit) = mean(remove_cont_array, 3); % remove if add quantisation metric
end




%%
% Quantisation constraint: pass registered images through normalised cross
% correlation to remove badly registered frames 

% reference image will be an averaged image of all images at a single
% exposure. For viewing, this is stored in 'folder'
folder = 'D:\Speckle\ID0Scan2\avg_exp';                                        %%
mkdir(folder);

% folder of all registered images and contrast maps passing normxcorr2() constraint
removed = 'D:\Speckle\ID0Scan2\frames_removed_xcorr';                      %% 
mkdir(removed);

contrast_removed = 'D:\Speckle\ID0Scan2\contrast_frames_removed_xcorr';    %%
mkdir(contrast_removed);

% create an averaged image of all registered images for each exposure
for i = exposures
    
    file_end1 = sprintf('*%04.1fms.pgm',i);
    first = dir(fullfile(reg_raw,file_end1)); 
   
    image = imread(fullfile(reg_raw,first(1).name)); 
    
    % mask the centre bright spot
    M = mean(mean(image));
    [m,n] = size(image);
    
    for c = floor(0.46*n) : floor(0.57*n)
        for r = floor(0.35*n) : floor(0.47*n)
            if image(r,c) > uint8(1)
                image(r,c) = uint8(M);
            end
        end
    end
    
    image = imadjust(image);  % Needed for dark images
    sumImage = double(image); % Initialise with the first image.

    % sum of all images at a single exposure
    parfor q = 2:numel(first)
        nextimage = imread(fullfile(reg_raw,first(q).name)); 
        
        % mask the centre bright spot
        M = mean(mean(nextimage));
        [m,n] = size(nextimage);

        for c = floor(0.46*n) : floor(0.57*n)
            for r = floor(0.35*n) : floor(0.47*n)
                if nextimage(r,c) > uint8(1)
                    nextimage(r,c) = uint8(M);
                end
            end
        end
        
        nextimage = imadjust(nextimage);
        sumImage = sumImage + double(nextimage); 
    end

    % display the average image (must be uint8)
    avgImage = uint8(sumImage / numel(first));
    %imshow(avgImage);
   
    [~,name,ext] = fileparts(first(1).name);
    F = fullfile(folder,strcat(name,ext));
    imwrite(avgImage,F);
end

% perform normalised cross correlation between a registered image and the
% averaged image

centralCorr = zeros(1,13);
finalCorr = zeros(1,13);

new_mat_maps = zeros(2152,2552,8);     % change third value for no of exp times     

% tell which contrast map to index from
i_count = 1;

for j = exposures
    
    % final contrast map array index
    f_count = 1;
    mat_maps_exp = zeros(2152,2552,1); % change third value for no of successful maps for each exp times     %%
    file_end2 = sprintf('*%04.1fms.pgm',j);
    
    % get directory of registered raw images of a single exposure
    first = dir(fullfile(reg_raw,file_end2));
    avgdir = dir(fullfile(folder, file_end2));
    ExpavgImage = imread(fullfile(folder,avgdir(1).name));
    %imshow(ExpavgImage);
    indexedContrast = dir(fullfile(contrastfolder,file_end2));
    
    % input to normxcorr2() should be of type double
    ExpavgImage = double(ExpavgImage);
    % know which row to place value in
    position = find(exposures == j);
    
    corrcell = zeros(1,numel(first));
    
    for o = 1: numel(first)
        imageName = first(o).name;
        crossImage = imread(fullfile(reg_raw,imageName)); 
        
        % mask the centre bright spot
        M = mean(mean(crossImage));
        [m,n] = size(crossImage);

        for c = floor(0.46*n) : floor(0.57*n)
            for r = floor(0.35*n) : floor(0.47*n)
                if crossImage(r,c) > uint8(1)
                    crossImage(r,c) = uint8(M);
                end
            end
        end
        
        crossImage = imadjust(crossImage);
        
        
        crossImage_double = double(crossImage); 
        
        corr = corr2(crossImage_double, ExpavgImage);
        
        % find the coloumn position on the array
        newStr = extractBefore(imageName,"_");
        column2 = (str2double(newStr) + 8 - (position - 1))/8;
        
        corrcell(1,column2) = corr;          
    end
    
    for y = 1:numel(first)
        % find the maximum value in the xcorrArray which corresponds to the
        % position of highest correlation (should be the middle as both images
        % are the same size)
        centralCorr(position,y) = corrcell(1,y);
        finalCorr(position,y) = corrcell(1,y);
    end
    
    x = centralCorr(position,:);
    expmean = mean(x(x>0.55));
    expstd = std(x(x>0.55));   

    for col = 1:numel(first)
        imageName2 = first(col).name;
        % find the coloumn position on the array
        newStr = extractBefore(imageName2,"_");
        column2 = (str2double(newStr) + 8 - (position - 1))/8;
        
        % make sure the number of values not zero in the array exceeds 9 or
        % else outlier will increase std and decrease mean
        if centralCorr(position,column2) > (expmean-3*expstd)
            % write raw and contrast images for gif
            storeImage = imread(fullfile(reg_raw,first(col).name)); 
            [~,name,ext] = fileparts(first(col).name);
            F = fullfile(removed,strcat(name,ext));
            imwrite(storeImage,F);
            
            [~,name,ext] = fileparts(indexedContrast(col).name);
            finalContrast = imread(fullfile(contrastfolder, indexedContrast(col).name));
            C = fullfile(contrast_removed,strcat(name,ext));
            imwrite(finalContrast,C);
            
            % store successful images in the array. f_count starts again
            % for each exposure so that the only images averaged are all 
            % of a single exposure
            mat_maps_exp(:,:,f_count) = remove_cont_array(:,:,i_count);
            f_count = f_count + 1;
            
        else
            finalCorr(position,column2) = 0;                             
        end   
        
        % i_count increases for the next image that is tested
        i_count = i_count + 1;
    end
    % averages all passed images of a single exposure
    new_mat_maps(:,:,position) = mean(mat_maps_exp, 3);
end

save('cont_maps_ID0Scan2_passXcorr.mat', 'new_mat_maps');                    %%%%%%%%%%%%%%%%
save('corr_ID0Scan2.mat', 'centralCorr', 'finalCorr');                       %%%%%%%%%%%%%%%%

% tell user that scan is useless if number of poorly registered
% frames/blinks is greater than 25% of the total number of frames
noOfZeros = nnz(~finalCorr);
if noOfZeros > 26
    disp('This scan contains many poorly registered frames/blinks. Please discard')
end

%%
% Successful images are written to a gif

% Specify the gif name for the raw images
gifname = 'D:\Speckle\ID0Scan2\raw_xcorr.gif';                               %% 

% Get all registered images from the newf folder
Directory = dir(fullfile(removed,'*.pgm')) ;                                 

for id = 1:numel(Directory)
    finalimage = imread(fullfile(removed,Directory(id).name)); % change reg rawfolder if add quantisation metric
    M = mean(mean(finalimage));
    [m,n] = size(finalimage);

    for c = floor(0.46*n) : floor(0.57*n)
        for r = floor(0.35*n) : floor(0.47*n)
            if finalimage(r,c) > uint8(1)
                finalimage(r,c) = uint8(M);
            end
        end
    end
    
    finalimage = imadjust(finalimage);
    % write the gif
    if id == 1
        imwrite(finalimage,gifname,'gif','LoopCount',Inf,'DelayTime',0.1);
    else
        imwrite(finalimage,gifname,'gif','WriteMode','append','DelayTime',0.1);
    end
end  

% Specify the gif name for the contrast images
gifname = 'D:\Speckle\ID0Scan2\contrast_xcorr.gif';                            %% 

% Get all registered images from the newf folder
Directory = dir(fullfile(contrast_removed,'*.pgm')) ;                       

for id = 1:numel(Directory)
    finalcontrast = imread(fullfile(contrast_removed,Directory(id).name)); % change folder if add quantisation metric

    % write the gif
    if id == 1
        imwrite(finalcontrast,gifname,'gif','LoopCount',Inf,'DelayTime',0.1);
    else
        imwrite(finalcontrast,gifname,'gif','WriteMode','append','DelayTime',0.1);
    end
end

% NewDirectory = dir(fullfile(contrast_removed,'*.pgm'));
% 
% array = zeros(646,766,104);
% 
% for id = 1:numel(NewDirectory)
%     finalcontrast = imread(fullfile(contrast_removed,NewDirectory(id).name));
%     array(:,:,id) = finalcontrast;
% end
% 
% figure, imshow(uint8(mean(array,3)));
