% This script calculates the contrast COV of the whole map. It first aligns
% all scans, then calculates COV pixel by pixel, then averages the COV of
% all pixels (with a circular mask). Alternatively, SSIM analysis of
% contrast maps can be done.

close all
clear

%% Initialise
% scan1 = load("D:\reg_mat_as_alldata\RR_4_1_1_reg_corr.mat").new_mat_maps;
% scan2 = load("D:\reg_mat_as_alldata\RR_4_1_2_reg_corr.mat").new_mat_maps;
% scan3 = load("D:\reg_mat_as_alldata\RR_4_1_3_reg_corr.mat").new_mat_maps;
% scan4 = load("D:\reg_mat_as_alldata\RR_4_1_4_reg_corr.mat").new_mat_maps;
% scan5 = load("D:\reg_mat_as_alldata\RR_4_1_5_reg_corr.mat").new_mat_maps;
% scan1 = load("D:\reg_mat_as_alldata\RR_2_1_1_unreg.mat").mat_maps;
% scan2 = load("D:\reg_mat_as_alldata\RR_2_1_2_unreg.mat").mat_maps;
% scan3 = load("D:\reg_mat_as_alldata\RR_2_1_3_unreg.mat").mat_maps;
% scan4 = load("D:\reg_mat_as_alldata\RR_2_1_4_unreg.mat").mat_maps;
% scan5 = load("D:\reg_mat_as_alldata\RR_2_1_5_unreg.mat").mat_maps;
% filename = 'D:\gifs\COV_8_test_16tform_5iter_aff_unreg.gif';
gif = 0;
COV = 0;
test_ssim = 1;
exp_times = [2 4 8 16 32 64];
reg_scans = zeros(2152,2552,5);
day = 1;
rego = 'unreg';
final_SSIM = zeros(9,6);

IDs = [0 1 2 3 4 5 7 8];

for id = IDs
    
    scan1_8exp = load('D:\unregistered_contrast\contrast_ID' + string(id) +'Scan1_noreg' + '.mat').new_mat_maps;
    scan2_8exp = load('D:\unregistered_contrast\contrast_ID' + string(id) +'Scan1_noreg' + '.mat').new_mat_maps;
    scan3_8exp = load('D:\unregistered_contrast\contrast_ID' + string(id) +'Scan1_noreg' + '.mat').new_mat_maps;
    scan4_8exp = load('D:\unregistered_contrast\contrast_ID' + string(id) +'Scan1_noreg' + '.mat').new_mat_maps;
    scan5_8exp = load('D:\unregistered_contrast\contrast_ID' + string(id) +'Scan1_noreg' + '.mat').new_mat_maps;
    
%     scan1 = load('D:\unreg_alldata\RR_' + string(id) +'_' + string(day) + '_1_' + rego + '.mat').mat_maps;
%     scan2 = load('D:\unreg_alldata\RR_' + string(id) +'_' + string(day) + '_2_' + rego + '.mat').mat_maps;
%     scan3 = load('D:\unreg_alldata\RR_' + string(id) +'_' + string(day) + '_3_' + rego + '.mat').mat_maps;
%     scan4 = load('D:\unreg_alldata\RR_' + string(id) +'_' + string(day) + '_4_' + rego + '.mat').mat_maps;
%     scan5 = load('D:\unreg_alldata\RR_' + string(id) +'_' + string(day) + '_5_' + rego + '.mat').mat_maps;
    
    scan1 = scan1_8exp(:,:,3:8);
    scan2 = scan2_8exp(:,:,3:8);
    scan3 = scan3_8exp(:,:,3:8);
    scan4 = scan4_8exp(:,:,3:8);
    scan5 = scan5_8exp(:,:,3:8);

    % Configure registration
    [optimizer, metric] = imregconfig('multimodal'); % Change to monomodal since the sensor is the same
    optimizer.MaximumIterations = 50; % Increase this to increase quality but decrease performance
    default = 3.90625e-4; % Return initial radius to normal value
    optimizer.InitialRadius = default;

    % Initialise transformation matrix
    tform = affine2d();
    tform_array = repmat(tform, [1 1 5]); % Could just change this to [numel(D)] in future
    tforms = zeros(3,3,5);

    all_scans = cat(3,scan1,scan2,scan3,scan4,scan5);

    %% Align all scans - registration should use the same tform for the same scan
    for iterations = 1:5 % Iterate 3 times

        if iterations == 1
            % Use 16ms scans as these seem to have the best image quality
            combined_scans = cat(3,scan1(:,:,4),scan2(:,:,4),scan3(:,:,4),scan4(:,:,4),scan5(:,:,4));
            fixed = mean(combined_scans,3); % average image
            fixed = imresize(fixed,0.3);
            r_and_c = size(fixed);
            small_cont_maps = zeros(r_and_c(1),r_and_c(2),5);
            moving_array = zeros(r_and_c(1),r_and_c(2),5);
        else
            fixed = mean(moving_array,3);
        end

        fixed = double(adapthisteq(uint8(fixed/max(max(fixed))*255)));
    %     figure,imshow(fixed,[])

        parfor k = 1:5 % For each of the 5 scans

            new_optimizer = optimizer;

            if iterations == 1 % Make the contrast maps the same size as the fixed image
                moving = combined_scans(:,:,k);
                moving = imresize(moving,0.3);
                small_cont_maps(:,:,k) = moving;
            end

            % Perform the registration
            tform = tform_array(:,:,k);
            tform = imregtform(small_cont_maps(:,:,k), fixed, 'affine', optimizer, metric,'InitialTransformation',tform);

            tform_array(:,:,k) = tform; % Write new tform into tform array  
        end

        for j = 1:numel(tform_array)
            if iterations < 5
                moving_array(:,:,j) = imwarp(small_cont_maps(:,:,j),tform_array(:,:,j),'OutputView',imref2d(size(fixed)));
    %             moving_array(:,:,k) = imwarp(small_cont_maps(:,:,k),tform,'OutputView',imref2d(size(fixed)));
            end    
        end
    end

    % Apply the same transform for all images in the same scan
    for scan = 1:5
        tform = tform_array(:,:,scan);
        scaled_tform = tform;
        scaled_tform.T(3,1:2) = tform.T(3,1:2)*2152/646;
        for num_exp = 1:length(exp_times)
            j = (scan-1)*6+num_exp;
            reg_scans(:,:,j) = imwarp(all_scans(:,:,j),scaled_tform,'OutputView',imref2d(size(all_scans(:,:,j))));
        end
    end
    disp("registration finished")

    %% Create a gif (to check registration)
    if gif == 1
        for idx = 1:30
            image = reg_scans(:,:,idx);

            % convert image into uint8 (as gif only accepts uint8)
            image_u8 = uint8(image*255/max(max(image)));
            image_u8 = imresize(image_u8,0.3);

            % write the gif
            if idx == 1
                imwrite(image_u8,filename,'gif','LoopCount',Inf,'DelayTime',0.1);
            else
                imwrite(image_u8,filename,'gif','WriteMode','append','DelayTime',0.1);
            end
        end
    end

    if COV == 1
        avg_COV = zeros(1,length(exp_times));

        for i = 1:length(exp_times)

            reg_scans_exp = zeros(391,471,5);
            count = 1;

            for l = i:6:30
                small_reg_scans = imresize(reg_scans(:,:,l),0.2);
                small_reg_scans = medfilt2(small_reg_scans,[5 5]); % Median filter to remove bright/dark spots
                reg_scans_exp(:,:,count) = small_reg_scans(20:410,20:490);
                count = count + 1;
            end

            %% For each pixel calculate COV = std/mean
            scans_std = std(reg_scans_exp,0,3);
            scans_mean = mean(reg_scans_exp,3);
            scans_COV = scans_std ./ scans_mean * 100;
    %         figure,imshow(scans_COV,[])

            %% Average COVs to find whole image variation, masking artifact
            scans_COV_nan = scans_COV;
            scans_COV_nan = circle_mask(scans_COV_nan,'NaN');
        %     scans_COV_nan = scans_COV_nan(20:410,20:490); % Remove edges
            avg_COV(i) = mean(scans_COV_nan,'all','omitnan');
            figure,imshow(scans_COV_nan,[0 20])

        end
    end

    %% SSIM
    if test_ssim == 1
        % for reference
        % FinalScan1 = reg_scans(:,:,1:6);
        % FinalScan2 = reg_scans(:,:,7:12);
        % FinalScan3 = reg_scans(:,:,13:18);
        % FinalScan4 = reg_scans(:,:,19:24);    
        % FinalScan5 = reg_scans(:,:,25:30);

        C = nchoosek([1,2,3,4,5],2);

        ssimvalarray = zeros(5,5,5);
        ssimmapcell = cell(5,5,5);
        ssimvalarray_circle = zeros(5,5,5);
        ssimmapcell_circle = cell(5,5,5);
        regscans = zeros(2152,2552,5);

        count2 = 1;

        for exp = 1:6

            for f = 1:5
                regscans(:,:,f) = reg_scans(:,:,(f-1)*6+exp);
            end   

            for h = 1:10
                image1 = regscans(:,:,C(h,1));
                image2 = regscans(:,:,C(h,2));

        %         figure, imshow(image1,[])
        %         colormap(jet)
        %         colorbar
        %         caxis([0 0.3])
        % 
        %         figure, imshow(image2,[])
        %         colormap(jet)
        %         colorbar
        %         caxis([0 0.3])

                [ssimval,ssimmap] = ssim(image1, image2);
        %         imshow(ssimmap)

                ssimmap_circle = ssimmap(50:end-50,50:end-50);
                ssimmap_circle = circle_mask(ssimmap_circle,'NaN');
                ssimval_circle = mean(ssimmap_circle,'all','omitnan');

                ssimvalarray(C(h,1),C(h,2),count2) = ssimval;
        %         ssimmapcell{C(h,1),C(h,2),count2} = ssimmap;

                ssimvalarray_circle(C(h,1),C(h,2),count2) = ssimval_circle;
        %         ssimmapcell_circle{C(h,1),C(h,2),count2} = ssimmap_circle;
            end
            count2 = count2 + 1;
        end

        exp_SSIM = zeros(1,6);

        scan_SSIM(1) = mean(ssimvalarray(1,2:5,:),'all');
        scan_SSIM(2) = mean(cat(2,ssimvalarray(2,3:5,:),ssimvalarray(1,2,:)),'all');
        scan_SSIM(3) = mean(cat(2,permute(ssimvalarray(1:2,3,:),[2 1 3]),ssimvalarray(3,4:5,:)),'all');
        scan_SSIM(4) = mean(cat(1,ssimvalarray(1:3,4,:),ssimvalarray(4,5,:)),'all');
        scan_SSIM(5) = mean(ssimvalarray(1:4,5,:),'all');

        ssimvalarray(ssimvalarray == 0) = NaN;
        exp_SSIM(1:6) = mean(ssimvalarray,[1 2],'omitnan');

        scan_SSIM_c(1) = mean(ssimvalarray_circle(1,2:5,:),'all');
        scan_SSIM_c(2) = mean(cat(2,ssimvalarray_circle(2,3:5,:),ssimvalarray_circle(1,2,:)),'all');
        scan_SSIM_c(3) = mean(cat(2,permute(ssimvalarray_circle(1:2,3,:),[2 1 3]),ssimvalarray_circle(3,4:5,:)),'all');
        scan_SSIM_c(4) = mean(cat(1,ssimvalarray_circle(1:3,4,:),ssimvalarray_circle(4,5,:)),'all');
        scan_SSIM_c(5) = mean(ssimvalarray_circle(1:4,5,:),'all');

        ssimvalarray_circle(ssimvalarray_circle == 0) = NaN;
        exp_SSIM_circle(1:6) = mean(ssimvalarray_circle,[1 2],'omitnan');

        final_SSIM(id+1,:)=exp_SSIM_circle;
%         f = msgbox('Operation Completed','Success');
    end
end