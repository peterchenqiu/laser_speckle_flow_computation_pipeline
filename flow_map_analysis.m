% This script analyses reproducibility in registered flow maps. The user
% may select a rectangular ROI, calculate whole map COVs or calculate whole
% map SSIMs (the last 2 with a circular mask and central artifact mask
% applied).

clear
close all

% Select options
rect_ROI = false;
wholemap_COV = true;
wholemap_SSIM = false;
avg_each_scan = false;
ID = 7;

% load files
scan1 = load('D:\data\SW_day1_2\ID' + string(ID) + 'Scan1Reg.mat').flow_map;
scan2 = load('D:\data\SW_day1_2\ID' + string(ID) + 'Scan2Reg.mat').flow_map;
scan3 = load('D:\data\SW_day1_2\ID' + string(ID) + 'Scan3Reg.mat').flow_map;
scan4 = load('D:\data\SW_day1_2\ID' + string(ID) + 'Scan4Reg.mat').flow_map;
scan5 = load('D:\data\SW_day1_2\ID' + string(ID) + 'Scan5Reg.mat').flow_map;
day_1 = cat(3,scan1,scan2,scan3,scan4,scan5);

scan6 = load('D:\data\SW_day1_2\ID' + string(ID) + 'Scan6Reg.mat').flow_map;
scan7 = load('D:\data\SW_day1_2\ID' + string(ID) + 'Scan7Reg.mat').flow_map;
scan8 = load('D:\data\SW_day1_2\ID' + string(ID) + 'Scan8Reg.mat').flow_map;
scan9 = load('D:\data\SW_day1_2\ID' + string(ID) + 'Scan9Reg.mat').flow_map;
scan10 = load('D:\data\SW_day1_2\ID' + string(ID) + 'Scan10Reg.mat').flow_map;
day_2 = cat(3,scan6,scan7,scan8,scan9,scan10);

%% Rectangular ROI
if rect_ROI == true

    % select a rectangle - double click to select
    I = scan1;
    
    imshow(I,[]);
    ell = drawrectangle('Position',[50 50 12 3],'Rotatable',true); % veins
%     ell = drawrectangle('Position',[50 50 6 2],'Rotatable',true); % arteries
    roi = customWait(ell);
    mask = createMask(roi);

    % create square around ellipse to crop image
    Stats = regionprops(mask,'Boundingbox');
    rect = rectangle('Position', Stats.BoundingBox).Position;
    rect_post = ell.Position;
    rect_rot = ell.RotationAngle;

    mask_cropped = imcrop(mask,rect);

    % initialise some arrays
    day_1_cropped = zeros(size(mask_cropped));
    day_2_cropped = zeros(size(mask_cropped));

    for i = 1:5
        I1 = day_1(:,:,i);   % Read image 
        I2 = day_2(:,:,i);   % Read image 

        I1 = imcrop(I1,rect);           % crop image 
        I2 = imcrop(I2,rect);

        day_1_cropped(:,:,i) = I1;
        day_2_cropped(:,:,i) = I2;
    end
    
    imshow(scan1,[])

    hold on;
    % rectangle('Position',rect,'EdgeColor','b')
    drawrectangle('Position',rect_post,'RotationAngle',rect_rot,'Color','r','InteractionsAllowed','none');
    
%     % ID2 vein - for visualisation
%     drawrectangle('Label','vein','LabelVisible','off','Position',[38.951621093750020,80.180937499999980,12,3],'RotationAngle',44.677188351075870,'Color','b','InteractionsAllowed','none');
%     
%     % ID2 artery - for visualisation
%     drawrectangle('Label','artery','LabelVisible','off','Position',[45.089376742325170,63.201285900502484,6,2],'RotationAngle',42.521458281336680,'Color','r','InteractionsAllowed','none');
%     aline = line(NaN,NaN,'LineWidth',2,'LineStyle','-','Color','r');
%     hline = line(NaN,NaN,'LineWidth',2,'LineStyle','-','Color','b');
%     legend('artery','vein')
    
    mask_cropped = double(mask_cropped);
    mask_cropped(mask_cropped == 0) = NaN;
    
    day1_vals = zeros(1,5);
    day2_vals = zeros(1,5);
    
    for j = 1:5
        
        % Mask the flow maps so that only values inside the ROI are
        % included
        masked_fm = mask_cropped.*day_1_cropped(:,:,j);
        masked_fm2 = mask_cropped.*day_2_cropped(:,:,j);
        
        scan_avg1 = mean(masked_fm,'all','omitnan');
        scan_avg2 = mean(masked_fm2,'all','omitnan');
        
        day1_vals(1,j) = scan_avg1;
        day2_vals(1,j) = scan_avg2;
    end
end

if wholemap_COV == true

    % For each pixel calculate COV = std/mean
    scans_std = std(day_1,0,3);
    scans_mean = mean(day_1,3);
    scans_COV = scans_std ./ scans_mean * 100;
%         figure,imshow(scans_COV,[])

    % Average COVs to find whole image variation, masking artifact
    scans_COV_nan = scans_COV;
    scans_COV_nan = circle_mask(scans_COV_nan,'NaN');
%     scans_COV_nan = scans_COV_nan(20:410,20:490); % Remove edges
    avg_COV = mean(scans_COV_nan,'all','omitnan');
%     figure,imshow(scans_COV_nan,[0 20])
%     colorbar
    
    % For each pixel calculate COV = std/mean
    scans_std = std(day_2,0,3);
    scans_mean2 = mean(day_2,3);
    scans_COV = scans_std ./ scans_mean2 * 100;
%         figure,imshow(scans_COV,[])

    % Average COVs to find whole image variation, masking artifact
    scans_COV_nan = scans_COV;
    scans_COV_nan = circle_mask(scans_COV_nan,'NaN');
%     scans_COV_nan = scans_COV_nan(20:410,20:490); % Remove edges
    avg_COV2 = mean(scans_COV_nan,'all','omitnan');
%     figure,imshow(scans_COV_nan,[0 30])
%     colorbar
    
    interday_rep = 100.*abs(scans_mean - scans_mean2)./((scans_mean + scans_mean2)./2);
    interday_rep_nan = circle_mask(interday_rep,'NaN');
    avg_idr = mean(interday_rep_nan,'all','omitnan');
    figure,imshow(interday_rep_nan,[0 60])
    colorbar
end

if avg_each_scan == true
    avg_day_1 = mean(day_1,'all');
%     avg_day_2 = mean(day_2,'all');
end

%% Functions to make drawing the ellipse easier - double click to continue
function pos = customWait(hROI)

% Listen for mouse clicks on the ROI
l = addlistener(hROI,'ROIClicked',@clickCallback);

% Block program execution
uiwait;

% Remove listener
delete(l);

% Return the current position
pos = hROI;

end

function clickCallback(~,evt)

if strcmp(evt.SelectionType,'double')
    uiresume;
end

end