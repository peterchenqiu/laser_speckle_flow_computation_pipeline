// 
// AndorCamera.h
// Author: Liam Murphy
// Created on: 2020-07-30
// 

#include "atcore.h"
#include "atutility.h"
#pragma once
#include "ICamera.h"
#include<iostream>
#include <exception>
#include<string>

#ifndef ACAMERA_H
#define ACAMERA_H

class AndorCamera :public ICamera
{
	public:
		AndorCamera();
		~AndorCamera();
		void open();
		void close();

		bool getCooling();
		void setCooling(bool state);
		double getTemperature();
		void setTemperature(double temp);

		void setExposureTime(double exp_time);
		double getExposureTime();

		double getFrameRate();
		void startVideo(unsigned char* initial_buffer);
		void stopVideo();
		void startSequence(int size);
		void addSequenceEntry(unsigned char* buf);
		void triggerFrame();
		unsigned char* getSequenceFrame();
		unsigned char* getFrame(unsigned char* buf, int buffer_size);
		int getSingleBufferSize();
		unsigned char* getSingleBuffer();

		int getWidth();
		int getHeight();
		int getStride();
	private:
		AT_H Hndl; //Camera Handle, used for any functions that interact with the camera
		double exposure_time;
		double frame_rate;
		AT_64 SingleImageSizeBytes;
		int i_SingleImageSizeBytes;
		bool is_cooling;
		double temperature;
		double target_temperature;
};

void validate(int i_retCode, std::string note);
#endif
