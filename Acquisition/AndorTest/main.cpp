﻿// 
// main.cpp (originally AndorTest.cpp)
// Author: Liam Murphy
// Created on: 2020-07-30
// 
#define _CRT_SECURE_NO_WARNINGS
#include <fstream>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <thread>
#include <future>
#include <numeric>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "ICamera.h"
#include "AndorCamera.h"
#include "atcore.h"
#include <windows.h>
#include <chrono>

using namespace std;

struct hud_vals
{
	double d_temperature;
	double d_exposuretime;
	double d_framerate;
};

struct min_max
{
	float min_exp;
	float max_exp;
};

struct session
//Contains information pertaining to specific scans.
{
	string patient_id; //unique identifier to a person.
	string session_id; //unique identifier to a specific scan for a given person. includes number and L/R eye.
	string patient_eye;
	double gradient;   //Gradient of linear fit of scan.
	double offset;     //offset of linear fit of scan.
	double r_sq;       //R Squared of linear fit of scan.
	string datetime;   //datetime of when the scan was executed.
	double duration;   //Duration of the sequence scan in ms.
	vector<double> frame_times; //vector containing the frame times. (Time each frame in a sequence took to setup and expose) Not just exposure time
	
	void write_session(string directory)
	{
		ofstream file;
		file.open(directory+"\\Session_" + patient_id + "_" + session_id + ".log");
		file << "Date time: " << datetime << endl;
		file << "Patient Id: " << patient_id << endl;
		file << "Patient Eye: " << patient_eye << endl;
		file << "Session Id: " << session_id << endl;
		file << "Gradient: " << to_string(gradient) << endl;
		file << "Offset: " << to_string(offset) << endl;
		file << "R Squared: " << to_string(r_sq) << endl;
		file << "Duration: " << to_string(duration) << " ms" << endl;
		file << "Frame Times:" << endl;

		for (unsigned int i = 0; i < frame_times.size(); i++)
		{
			file << frame_times[i] << endl;
		}

		file.close();
	}
};

struct fit_params
{
	double gradient;
	double offset;
	double r_sq;
};

fit_params linear_fit(vector<double> x, vector<double> y);
void vitals_thread(ICamera* cam, hud_vals& hud, bool& flag);
unsigned char** sequence_display(ICamera* camera, const int width, const int height, const int stride, const int no_frames, unsigned char** expected_buffers);
void save_image(unsigned char* image_buffer, string filename, const int width, const int height, const int stride);
void sequence(ICamera* camera, const int width, const int height, const int stride, session new_session);
bool display_loop(ICamera* camera, const int cols, const int rows, const int stride);
void buffer_to_mat(cv::Mat& display_image, unsigned char* display_buffer, const int cols, const int rows, const int stride);
double image_array_mean(unsigned char* pixel_array, int height, int width, int stride);
string get_current_datetime();


int main(int argc, char* argv[])
{
	cout << "----------------------------------\nLaser Speckle Fundus Imager (LSFI)\n----------------------------------\n\n" << endl;
	ICamera* camera = new AndorCamera();
	camera->setTemperature(-20);
	camera->setCooling(true); // for noise pollution purposes
	const int rows = camera->getHeight();
	const int cols = camera->getWidth();
	const int stride = camera->getStride();

	while (true)
	{
		session new_session;

		cout << "\n\nPlease enter patient id: (q to exit): " ;
		getline(cin, new_session.patient_id);
		if (new_session.patient_id == "q") break;
		cout << "Please enter patient eye (L or R)";
		getline(cin, new_session.patient_eye);
		cout << "Please enter session id:" ;
		getline(cin, new_session.session_id);

		bool should_continue = display_loop(camera, cols, rows, stride); // starts the opencv video loop, returns once user hits spacebar and patient is centered.
		if (should_continue) sequence(camera, cols, rows, stride, new_session); //starts the measurement sequence and saves the data.
	}

	camera->close();
	delete camera;

	return 0;
}


void vitals_thread(ICamera* cam, hud_vals &hud, bool& flag)
// this polls the camera at a rate of 2Hz for the values of interest
{
	do
	{
		Sleep(500);
		hud.d_temperature = cam->getTemperature();
		hud.d_exposuretime = cam->getExposureTime();
		hud.d_framerate = cam->getFrameRate();
	} while (flag);
}

string get_current_datetime()
//returns the current datetime as a string in the format YYYY-MM-DD_HH-MM-SS
{
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);

	std::ostringstream oss;
	oss << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S");

	return oss.str();
}

void buffer_to_mat(cv::Mat &display_image, unsigned char* display_buffer, const int cols, const int rows, const int stride)
{
	for (int y = 0; y < rows; y++)
	{
		unsigned short* ImagePixels = reinterpret_cast<unsigned short*>(display_buffer);
		for (int x = 0; x < cols; x++)
		{
			display_image.at<unsigned short>(y, x) = (unsigned short)ImagePixels[x];
		}
		display_buffer += stride;
	}
}

bool display_loop(ICamera* camera, const int cols, const int rows, const int stride)
// Using camera, acquiries a video loop, displayed live with opencv, basic I/O here, handels a device polling thread.
{
	hud_vals hud = hud_vals();
	bool thread_flag = true;
	thread vitals(vitals_thread, camera, ref(hud), ref(thread_flag));

	unsigned char* UserBuffer = camera->getSingleBuffer();
	camera->setExposureTime(15.0);
	camera->startVideo(UserBuffer);
	cout << "Waiting for acquisition ..." << endl << endl;

	unsigned char* displayBuffer;
	cv::Mat display_image = cv::Mat::zeros(rows, cols, CV_16UC1);
	string window_title = "Laser Speckle Fundus Imager (LSFI)";
	cv::namedWindow(window_title, cv::WINDOW_AUTOSIZE);
	double gamma = .75; // 2.2;
	bool gam_changed = true;
	cv::Mat lookUpTable(1, 65536, CV_8U);

	bool return_value = false;
	bool mask_on = true;
	while (true)
	{
		displayBuffer = camera->getFrame(UserBuffer, camera->getSingleBufferSize());
		buffer_to_mat(display_image, displayBuffer, cols, rows, stride);
		

		////////// ARTEFACT MASKING /////////////////
		if (mask_on)
		{
			for (int x = 1130; x < 1525; x++)
			{
				for (int y = 860; y < 1265; y++)
				{
					display_image.at<ushort>(y, x) = 0;
				}
			}

		}

		/////////////////////////////////////////////


		cv::Mat stat_img(display_image, cv::Rect(320, 256, 640, 512));
		double minVal;
		double maxVal;
		cv::Point minLoc;
		cv::Point maxLoc;
		cv::minMaxLoc(stat_img, &minVal, &maxVal, &minLoc, &maxLoc);
		cv::Scalar tempVal = cv::mean(stat_img);
		double meanVal = tempVal.val[0];

		cv::Mat img_dst;
		cv::resize(display_image, img_dst, cv::Size(800, 600), 0, 0, cv::INTER_AREA);

		cv::Mat img_dst_normed;
		cv::normalize(img_dst, img_dst_normed, 0, 65535, cv::NORM_MINMAX);
		cv::Mat lutted = cv::Mat(600, 800, CV_8UC1);
		
		//creating the lut
		uchar* p = lookUpTable.ptr();
		if (gam_changed)
		{
			for (int i = 0; i < 65536; ++i)
			{
				/*
				if (i/16> maxVal-150)
				{
					p[i] = 0;
				}
				else
				{*/
				p[i] = cv::saturate_cast<uchar>(pow(i / 65535.0, gamma) * 255);
				//}
				
			}
				
				
				
			gam_changed = false;
		}
		
		//applying the lut
		uchar* p2 = lutted.ptr();
		ushort* p3 = img_dst_normed.ptr<ushort>();
		for (int i = 0; i < 800 * 600; ++i)
			p2[i] = p[p3[i]];
		
		cv::Mat flipped;
		cv::flip(lutted, flipped, 1);

		//Adding the bottom border for readout values
		cv::Mat img_dst_border = cv::Mat::zeros(660, 800, CV_8UC1);
		cv::copyMakeBorder(flipped, img_dst_border, 0, 60, 0, 0, 0, 0);

		cv::Mat rgb_display_image;
		cv::cvtColor(img_dst_border, rgb_display_image, cv::COLOR_GRAY2RGB);

		cv::putText(rgb_display_image,
			to_string((int)hud.d_exposuretime) + " ms     " + to_string((int)hud.d_temperature) + " *C     " + to_string((int)hud.d_framerate) + " Hz     gam: " + to_string(gamma),
			cv::Point(15, 623),
			cv::FONT_HERSHEY_DUPLEX,
			0.75,
			CV_RGB(255, 255, 255),
			1);

		cv::putText(rgb_display_image,
			"Max Value: " + to_string(int(maxVal)),
			cv::Point(15, 653),
			cv::FONT_HERSHEY_DUPLEX,
			0.75,
			CV_RGB(255, 255, 255),
			1);

		cv::Scalar traffic_light(0, 255, 0, 255);
		traffic_light[3] = 255;
		traffic_light[1] = 255;
		if (meanVal < 600)
		{
			traffic_light[2] = 255;
			if (meanVal < 300)traffic_light[1] = 0;
			else traffic_light[1] == 200;
		}

		cv::putText(rgb_display_image,
			"Mean Value: " + to_string(int(meanVal)),
			cv::Point(350, 653),
			cv::FONT_HERSHEY_DUPLEX,
			0.75,
			traffic_light, //CV_RGB(255, 255, 255),
			1);
		
		//show image and take in keyboard input
		cv::imshow(window_title, rgb_display_image);

		int key = cv::waitKey(10);

		if (key == 113) // q
		{
			thread_flag = false;
			vitals.join();
			cv::destroyAllWindows();
			break;
		}
		else if (key == 110) // n
		{
			camera->setExposureTime(camera->getExposureTime() - 1);
		}
		else if (key == 109) // m
		{
			camera->setExposureTime(camera->getExposureTime() + 1);
		}
		else if (key == 108) // l
		{
			gamma += 0.02;
			gam_changed = true;
		}
		else if (key == 107) // k
		{
			gamma -= 0.02;
			gam_changed = true;
		}
		else if (key == 32)// space bar
		{
			cv::destroyAllWindows();
			camera->stopVideo();
			thread_flag = false;
			vitals.join();
			return_value = true;
			break;
		}
		else if (key == 106)// J
		{
			mask_on = !mask_on;
		}
	}

	delete[] UserBuffer;
	camera->stopVideo();

	return return_value;
}

void save_image(unsigned char* image_buffer, string filename, const int width, const int height, const int stride)
//Takes image buffer pointer and filename, saves a 16-bit pgm in big endian format.
{
	{
		//pgm 16-bit image saving method
		std::ofstream f(filename, std::ios_base::out
			| std::ios_base::binary
			| std::ios_base::trunc
		);

		int maxColorValue = 65535;
		f << "P5\n" << width << " " << height << "\n" << maxColorValue << "\n"; // this is the pgm header.

		for (int i = 0; i < height; ++i)
		{
			for (int k = 0; k < width; ++k)
			{
				//We convert the little endian data into big endian format.
				unsigned short* Image_pixels = reinterpret_cast<unsigned short*>(image_buffer);
				unsigned char MSB = Image_pixels[k] >> 8;
				unsigned char LSB = Image_pixels[k] & 255;
				image_buffer[2 * k] = MSB;
				image_buffer[2 * k + 1] = LSB;
			}
			//write by the row for efficiency.
			f.write(reinterpret_cast<const char*>(image_buffer), 2 * width);
			image_buffer += stride;
		}
	}
}

double image_array_mean(unsigned char* pixel_array, int height, int width, int stride)
// Takes in a unsigned char array and computes the mean and returns it.
{
	//int size = (int)((double)sizeof(pixel_array)/(double)sizeof(pixel_array[0]));
	double sum = 0;
	for (int i = 0; i < height; ++i)
	{
		for (int k = 0; k < width; ++k)
		{

			unsigned short* Image_pixels = reinterpret_cast<unsigned short*>(pixel_array);

			sum += Image_pixels[k];
		}
		pixel_array += stride;
	}
	return sum / (width * height);
}

unsigned char** sequence_display(ICamera* camera, const int width, const int height, const int stride, const int no_frames, unsigned char** expected_buffers)
//Called in a different thread, displays the 
{
	unsigned char* display_buffer;
	cv::Mat display_image = cv::Mat::zeros(height, width, CV_16UC1);
	string window_title = "Now Scanning...";
	cv::namedWindow(window_title, cv::WINDOW_AUTOSIZE);
	unsigned char** displayed_buffers = new unsigned char* [no_frames + 1]; //array of pointers to buffers taken and displayed
	
	for (int i = 0; i < no_frames; i++)
	{
		display_buffer = camera->getSequenceFrame();
		displayed_buffers[i] = display_buffer;
		if (display_buffer != expected_buffers[i])
		{
			cout << "Failed to get Image: " << i << endl;
		}
		else
		{
			buffer_to_mat(display_image, display_buffer, width, height, stride);
			cv::Mat img_dst;
			cv::resize(display_image, img_dst, cv::Size(800, 600), 0, 0, cv::INTER_AREA);

			cv::Mat img_dst_normed;
			cv::normalize(img_dst, img_dst_normed, 0, 65535, cv::NORM_MINMAX);

			cv::Mat flipped;
			cv::flip(img_dst_normed, flipped, 1);
			cv::imshow(window_title, flipped);
			int key = cv::waitKey(1);
		}
	}

	cv::destroyAllWindows();
	return displayed_buffers;
}

min_max get_exposure_times(ICamera* camera, const int width, const int height, const int stride)
{
	min_max a;
	int no_exps = 3;
	unsigned char** image_buffers = new unsigned char* [no_exps + 1]; //we assign an extra buffer just for the end of the video loop.
	vector<double> mini_seq{ 10, 15, 20 };
	for (unsigned int i = 0; i < no_exps; i++)
	{
		image_buffers[i] = camera->getSingleBuffer();
		camera->addSequenceEntry(image_buffers[i]);

		
	}
	for (unsigned int i = 0; i < no_exps; i++)
	{
		camera->setExposureTime(mini_seq[i]);

		Sleep(1); // This sleep is potentially needed to give the camera to get to the correct state
		camera->triggerFrame();
		Sleep(mini_seq[i]);
	}
	vector<double> mean_values;
	vector<double> exposure_times;

	//Now grabs the frames from memory and performs regressions and saves the images.
	for (unsigned int i = 0; i < mini_seq.size(); i++)
	{
		unsigned char* displayBuffer;
		displayBuffer = camera->getSequenceFrame();
		if (image_buffers[i] != displayBuffer)
		{
			cout << "Failed to get Image: " << i << ", exp time: " << mini_seq[i] << endl;
		}
		else
		{
			double mean = image_array_mean(displayBuffer, height, width, stride);
			mean_values.push_back(mean);
			exposure_times.push_back(mini_seq[i]);
		}
	}

	fit_params fit_results = linear_fit(exposure_times, mean_values);
	for (unsigned int i = 0; i < no_exps + 1; i++)
	{
		delete[] image_buffers[i];
	}

	float min_value = 200;
	float max_value = 1900;
	a.min_exp = (min_value - fit_results.offset) / fit_results.gradient;
	a.max_exp = (max_value - fit_results.offset) / fit_results.gradient;
	return a;
}


void sequence(ICamera* camera, const int width, const int height, const int stride, session new_session)
// Reads the sequence.txt file, allocates and queues the buffers for the sequence, acquires and then saves the images, performs regression on mean value of images too and gives gradient/offset.
{
	//min_max exp_struct = get_exposure_times(camera, width, height, stride);
	//read the sequence.txt file.
	vector<double> exposure_sequence; //each element represents an exposure, the value of that element is its exposure time.
	ifstream infile("sequence.txt");
	double a;
	while (infile >> a)
	{
		exposure_sequence.push_back(a);
	}
	infile.close();
	/*
	int no_exp_times = 8;
	
	int no_repeats = 8;
	double exp_time_step = (exp_struct.max_exp - exp_struct.min_exp) / no_exp_times;
	for (unsigned int k = 0; k < no_repeats; k++)
	{
		for (unsigned int i = 0; i < no_exp_times; i++)
		{
			exposure_sequence.push_back(exp_struct.min_exp + i * exp_time_step);
		}
	}*/

	//Allocate memory buffers
	unsigned char** image_buffers = new unsigned char* [exposure_sequence.size()+1]; //we assign an extra buffer just for the end of the video loop.
	for (unsigned int i = 0; i < exposure_sequence.size(); i++)
	{
		image_buffers[i] = camera->getSingleBuffer();
	}

	for (unsigned int i = 0; i < exposure_sequence.size(); i++)
	{
		camera->addSequenceEntry(image_buffers[i]);
	}

	camera->setExposureTime(exposure_sequence[0]);
	camera->startSequence(exposure_sequence.size());
	
	cout << "Framerate: " << camera->getFrameRate()<< " Hz" << endl;

	Sleep(5);// This sleep is potentially needed to give the camera to get to the correct state
	

	auto sequence_display_thread = async(sequence_display, camera, width, height, stride, exposure_sequence.size(), image_buffers);
	//sequence triggering
	auto seq_start = chrono::steady_clock::now();
	

	for (unsigned int i = 0; i < exposure_sequence.size(); i++)
	{
		auto cam_set_exp_start = chrono::steady_clock::now();
		camera->setExposureTime(exposure_sequence[i]);
		
		Sleep(1); // This sleep is potentially needed to give the camera to get to the correct state
		camera->triggerFrame();
		//Sleep(5);
		Sleep(exposure_sequence[i]);
		auto cam_set_exp_stop = chrono::steady_clock::now();
		new_session.frame_times.push_back((cam_set_exp_stop - cam_set_exp_start) / chrono::milliseconds(1));
	}
	auto seq_end = chrono::steady_clock::now();
	unsigned char** displayed_buffers= sequence_display_thread.get();
	
	double duration = (seq_end - seq_start) / chrono::milliseconds(1);
	new_session.duration = duration;
	cout << "Sequence acquired in: " << duration << " (ms)" << endl;
	cout << "Sequence Acquired, now saving..." << endl;
	string datetime_str = get_current_datetime();

	string directory = "Sequences\\"+datetime_str + "_Sequence_"+new_session.patient_id+"_"+new_session.session_id;
	if (!(CreateDirectory(directory.c_str(), NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError()))
	{
		cout << "Unable to create save directory" << endl;
		directory = "";
	}

	vector<double> mean_values;
	vector<double> exposure_times;

	//Now grabs the frames from memory and performs regressions and saves the images.
	for (unsigned int i = 0; i < exposure_sequence.size(); i++)
	{
		unsigned char* displayBuffer;
		displayBuffer = displayed_buffers[i];
		if (image_buffers[i] != displayBuffer)
		{
			cout << "Failed to get Image: " << i << ", exp time: " << exposure_sequence[i] << endl;
		}
		else
		{
			double mean = image_array_mean(displayBuffer, height, width, stride);
			mean_values.push_back(mean);
			exposure_times.push_back(exposure_sequence[i]);

			stringstream ss, sss;
			ss << setw(5) << setfill('0') << std::fixed << std::setprecision(1) << exposure_sequence[i]; //this is the culprit
			string exp_time = ss.str();
			
			sss << setw(3) << setfill('0') << i;
			string img_indice = sss.str();
			save_image(image_buffers[i], directory + "\\" + img_indice + "_sequence_" + datetime_str + "_"+ exp_time + "ms.pgm", width, height, stride);
		}
	}

	fit_params fit_results = linear_fit(exposure_times, mean_values);

	new_session.gradient = fit_results.gradient;
	new_session.offset = fit_results.offset;
	new_session.r_sq = fit_results.r_sq;
	new_session.datetime = datetime_str;

	new_session.write_session(directory);
	cout << "Saving complete!" << endl;
	//free the memory!
	for (unsigned int i = 0; i < exposure_sequence.size(); i++)
	{
		delete[] image_buffers[i];
	}

	delete[] image_buffers;
	delete[] displayed_buffers;
	camera->stopVideo();
}

fit_params linear_fit(vector<double> x, vector<double> y)
{
	/* Simple Least Squares Implementation, used to tell if the data is of good quality or not.*/
	int n = y.size();
	double mean_y = (double)accumulate(y.begin(), y.end(), 0) / n;
	double mean_x = (double)accumulate(x.begin(), x.end(), 0) / n;
	cout << "Mean X: " << mean_x << endl;
	cout << "Mean Y: " << mean_y << endl;

	double numerator = 0;
	double denominator = 0;
	double ss_tot = 0;
	double ss_res = 0;

	for (int i = 0; i < n; i++)
	{
		double dif_y = y[i] - mean_y;
		double dif_x = x[i] - mean_x;
		numerator += (dif_y * dif_x);
		denominator += (dif_x * dif_x);
		ss_tot += (dif_y * dif_y);

	}

	double gradient = numerator / denominator;
	double offset = mean_y - gradient * mean_x;

	for (int i = 0; i < n; i++)
	{
		double val = y[i] - (x[i] * gradient + offset);
		ss_res += val * val;
	}

	double r_sq = 1 - ss_res / ss_tot;
	cout << "Gradient: " << gradient << endl;
	cout << "Offset: " << offset << endl;
	cout << "R Squared: " << r_sq << endl;

	fit_params result { gradient, offset, r_sq };
	return result;
}