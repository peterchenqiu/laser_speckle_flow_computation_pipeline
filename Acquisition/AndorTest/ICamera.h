// 
// ICamera.h
// Author: Liam Murphy
// Created on: 2020-07-30
// Camera Interface
//

#pragma once
#ifndef ICAMERA_H
#define ICAMERA_H
class ICamera
{
	public:
		virtual void open() = 0;	//Connect/Creation of camera handle. Could be called in constructor.
		virtual void close() = 0;	//Terminates any activity and camera handle. Could be called in destructor.
		virtual bool getCooling() = 0;	//Gets the state of cooling as either on (true) or off (false).
		virtual void setCooling(bool state) = 0;	//Sets the state of cooling to either on (true) or off (false).
		virtual double getTemperature() = 0;	//Returns the temperature in units of degrees celsisus.
		virtual void setTemperature(double temp) = 0;	//Sets the temperture in units of degrees celsisus.

		virtual void setExposureTime(double exp_time) = 0;	//Sets the length of exposure time in units of milliseconds.
		virtual double getExposureTime() = 0;	// returns the exposure time in units of milliseconds.
		virtual double getFrameRate() = 0;

		virtual void startVideo(unsigned char* initial_buffer) = 0;
		virtual void stopVideo() = 0;
		virtual void startSequence(int size) = 0;
		virtual void addSequenceEntry(unsigned char* buf) = 0;
		virtual void triggerFrame() = 0;
		virtual unsigned char* getSequenceFrame() = 0;
		virtual unsigned char* getFrame(unsigned char* buf, int buffer_size) = 0;
		virtual int getSingleBufferSize() = 0;	//return the number of bytes required for a buffer to store an image
		virtual unsigned char* getSingleBuffer() = 0; // returns a pointer to a newly allocated chunk of memory to hold a single image
		virtual int getWidth() = 0;
		virtual int getHeight() = 0;
		virtual int getStride() = 0;
	private:

};

#endif

