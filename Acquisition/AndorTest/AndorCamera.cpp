// 
// AndorCamera.cpp
// Author: Liam Murphy
// Created on: 2020-07-30
// Implementation of the ICamera interface, using the Andor SDK3.
//

#include "AndorCamera.h"

/*
Full Frame Settings
Image Size (bytes): 11061360
Row Size (bytes): 5121
No. rows: 2160
No. Cols: 2560*/


AndorCamera::AndorCamera()
{
#pragma region Camera_Specific_Settings
	int i_retCode = AT_InitialiseLibrary();
	if (i_retCode != AT_SUCCESS) {
		std::cout << "Error initialising Andor library" << std::endl << std::endl;
		throw(std::runtime_error::runtime_error("Error Loading Andor Library!"));
	}

	open();

	i_retCode = AT_SetEnumerated(Hndl, L"SimplePreAmpGainControl", 2);
	if (i_retCode != AT_SUCCESS) std::cout << "Unable to set PreAmpGainControl, retcode: " << i_retCode << std::endl;

	i_retCode = AT_SetBool(Hndl, L"SpuriousNoiseFilter", AT_FALSE);
	if (i_retCode != AT_SUCCESS) std::cout << "Unable to set Noise Filter, retcode: " << i_retCode << std::endl;

	i_retCode = AT_SetEnumeratedString(Hndl, L"ElectronicShutteringMode", L"Rolling");
	if (i_retCode != AT_SUCCESS) std::cout << "Unable to set shutter mode, retcode: " << i_retCode << std::endl;

	AT_64 accu_count;
	i_retCode = AT_GetInt(Hndl, L"AccumulateCount", &accu_count);
	std::cout << "accumulate count: " << (int)accu_count << std::endl;

	// NOTE: SETTING MONO16 will CHANGE GAIN SETTING, SETTING GAIN to 1 (as above) sets PIXEL ENCODING TO MONO 12/PACKED!
	i_retCode = AT_SetEnumeratedString(Hndl, L"PixelEncoding", L"Mono16");
	if (i_retCode != AT_SUCCESS) std::cout << "Unable to set Pixel Encoding, retcode: " << i_retCode << std::endl;

	int encoding;
	i_retCode = AT_GetEnumIndex(Hndl, L"PixelEncoding", &encoding);
	std::cout << "Pixel Encoding enum: " << encoding << std::endl;

	int gain_enum;
	i_retCode = AT_GetEnumIndex(Hndl, L"SimplePreAmpGainControl", &gain_enum);
	if (i_retCode != AT_SUCCESS) std::cout << "Unable to get PreAmpGainControl, retcode: " << i_retCode << std::endl;
	std::cout << "SimpleGain Setting enum: " << gain_enum << std::endl;

	i_retCode = AT_GetInt(Hndl, L"Image Size Bytes", &SingleImageSizeBytes);
	if (i_retCode == AT_SUCCESS) i_SingleImageSizeBytes = static_cast<int>(SingleImageSizeBytes);
	else i_SingleImageSizeBytes = -1;

	i_retCode = AT_SetEnumeratedString(Hndl, L"PixelReadoutRate", L"280 MHz");
	if (i_retCode != AT_SUCCESS)  std::cout << "Unable to set Pixel Readout Rate retcode: " << i_retCode << std::endl;

	i_retCode = AT_SetEnumString(Hndl, L"TriggerMode", L"Software");
	validate(i_retCode, "Unable to set trigger mode, retcode: ");

	i_retCode == AT_SetBool(Hndl, L"Overlap", AT_TRUE);
	validate(i_retCode, "Unable to set overlap, retcode: ");
#pragma endregion Camera_Specific_Settings

	int val;
	AT_GetEnumIndex(Hndl, L"TriggerMode", &val);
	std::cout << "Trigger mode is: " << val << std::endl;
	setExposureTime(35);

	i_retCode = AT_SetFloat(Hndl, L"FrameRate", 25);//this has to be a valid number relative to the exposure time, otherwise it throws exception
	validate(i_retCode, "Unable to set frame rate, retcode: ");
}

AndorCamera::~AndorCamera()
{
	AT_FinaliseLibrary();
	AT_FinaliseUtilityLibrary();
}

void AndorCamera::open()
{
	int i_retCode = AT_Open(0, &Hndl);
	if (i_retCode != AT_SUCCESS) { throw(std::runtime_error::runtime_error("Error Connecting to Andor Camera!")); }
}

void AndorCamera::close()
{
	AT_Close(Hndl);
}

bool AndorCamera::getCooling()
{
	return is_cooling;
}

void AndorCamera::setCooling(bool state)
{
	int i_retCode;
	if (state) 
	{
		i_retCode = AT_SetFloat(Hndl, L"TargetSensorTemperature", target_temperature);
		validate(i_retCode, "Unable to set target temp, retcode: ");
		i_retCode = AT_SetBool(Hndl, L"SensorCooling", AT_TRUE);
		validate(i_retCode, "Unable to set cooling on, retcode: ");
		i_retCode = AT_SetEnumeratedString(Hndl, L"FanSpeed", L"On");
		validate(i_retCode, "Unable to set fanspeed low, retcode: ");
	
		is_cooling = true;
	}
	else
	{
		i_retCode = AT_SetBool(Hndl, L"SensorCooling", AT_FALSE);
		validate(i_retCode, "Unable to set cooling off, retcode: ");
		i_retCode = AT_SetEnumeratedString(Hndl, L"FanSpeed", L"Low");
		validate(i_retCode, "Unable to set fanspeed off, retcode: ");
	
		is_cooling = false;
	}
}

double AndorCamera::getTemperature()
{
	int i_retCode = AT_GetFloat(Hndl, L"SensorTemperature", &temperature);
	validate(i_retCode, "Unable to get temperature, retcode: ");
	return temperature;
}

void AndorCamera::setTemperature(double temp)
{
	int i_retCode = AT_SetFloat(Hndl, L"Temperature", temp);
	target_temperature = temp;
	validate(i_retCode, "Unable to set temperature, retCode: ");
}

void AndorCamera::setExposureTime(double exp_time)
{
	int i_retCode = AT_SetFloat(Hndl, L"Exposure Time", exp_time/1000.0);
	validate(i_retCode, "Unable to set exposure time, retCode: ");
	i_retCode = AT_GetFloat(Hndl, L"Exposure Time", &exposure_time);
	validate(i_retCode, "Unable to get exposure time, retCode: ");
	exposure_time *= 1000;

	double fps_max;
	i_retCode = AT_GetFloatMax(Hndl, L"FrameRate", &fps_max);
	validate(i_retCode, "Unable to get max frame rate, retCode: ");
	frame_rate = fps_max;
	i_retCode = AT_SetFloat(Hndl, L"FrameRate", frame_rate);
	validate(i_retCode, "Unable to set frame rate, retCode: ");
}

double AndorCamera::getExposureTime()
{
	return exposure_time;
}

void AndorCamera::startVideo(unsigned char* initial_buffer)
{
	AT_Flush(Hndl);
	int iret = AT_SetEnumeratedString(Hndl, L"CycleMode", L"Continuous");
	int iret2 = AT_Command(Hndl, L"Acquisition Start");
	if (iret ==AT_SUCCESS && iret2 == AT_SUCCESS) std::cout << "Started video mode" << std::endl;
	iret = AT_QueueBuffer(Hndl, initial_buffer, getSingleBufferSize());
	validate(iret, "Unable to queue the first buffer, retcode: ");
}

void AndorCamera::stopVideo()
{
	AT_Command(Hndl, L"Acquisition Stop");
	AT_Flush(Hndl);
}

void AndorCamera::addSequenceEntry(unsigned char* buf)
// adds the allocated buffer to the queue
{
	int iret = AT_QueueBuffer(Hndl, buf, getSingleBufferSize());
	validate(iret, "Unable to queue the first buffer, retcode: ");
}

void AndorCamera::startSequence(int size)
// Sets camera to sequence mode
{
	int iret = AT_SetEnumeratedString(Hndl, L"CycleMode", L"Fixed");
	validate(iret, "Unable to set cyclemode to fixed, retcode: ");
	iret = AT_SetInt(Hndl, L"FrameCount", (AT_64)size);
	validate(iret, "Unable to set framecount, retcode: ");
	iret = AT_Command(Hndl, L"Acquisition Start");
	validate(iret, "Unable to start acquisition, retcode: ");
	
	double maxRate;
	iret = AT_GetFloat(Hndl, L"MaxInterfaceTransferRate", &maxRate);
	std::cout << "Max transfer rate is: " << maxRate << " Hz" << std::endl;
}

void AndorCamera::triggerFrame()
// Sends command to camera to acquire a frame
{
	int i_retCode = AT_Command(Hndl, L"SoftwareTrigger");
	validate(i_retCode, "Unable to execute software trigger, retcode: ");
}

unsigned char* AndorCamera::getSequenceFrame()
// grabs recently acquired frame from the sequence queue
{
	int i_retCode;
	unsigned char* Buffer;
	int buffer_a_size = getSingleBufferSize();
	i_retCode = AT_WaitBuffer(Hndl, &Buffer, &buffer_a_size, 1000);
	validate(i_retCode, "Unable to retrieve frame, retcode: ");
	return Buffer;
}

unsigned char*  AndorCamera::getFrame(unsigned char* buf, int buffer_size)
// Acquires a frame, and then queues the parsed buffer.
// This is done to keep a loop running for video display.
{
	AT_Command(Hndl, L"SoftwareTrigger");
	unsigned char* Buffer;
	int buffer_a_size = getSingleBufferSize();
	int i_retCode = AT_WaitBuffer(Hndl, &Buffer, &buffer_a_size, AT_INFINITE);
	validate(i_retCode, "Unable to retrieve frame, retcode: ");
	i_retCode = AT_QueueBuffer(Hndl, buf, buffer_size);
	validate(i_retCode, "Unable to queue frame, retcode: ");

	return Buffer;
}

unsigned char* AndorCamera::getSingleBuffer()
// returns a pointer to an allocated image buffer in heap memory.
{
	 unsigned char* buffer = new unsigned char[i_SingleImageSizeBytes];
	//unsigned char* alignedBuffer = reinterpret_cast<unsigned char*>((reinterpret_cast<unsigned long>(buffer) + 7) & ~0x7);
	
	return buffer;
}

int AndorCamera::getSingleBufferSize()
// returns the number of bytes acquired in a single image
{
	return i_SingleImageSizeBytes;
}

int AndorCamera::getWidth()
// returns the number of pixels in a row
{
	AT_64 width;
	int i_retCode = AT_GetInt(Hndl, L"AOIWidth", &width);
	validate(i_retCode, "Unable to get width value, retcode: ");
	return (int) width;
}

int AndorCamera::getHeight()
// returns the number of pixels in a column
{
	AT_64 height;
	int i_retCode = AT_GetInt(Hndl, L"AOIHeight", &height);
	validate(i_retCode, "Unable to get Height value, retcode: ");
	return (int)height;
}

int AndorCamera::getStride()
// returns the number of bytes in a row of the image. (bytes of pixels + margin)
{
	AT_64 stride;
	int i_retCode = AT_GetInt(Hndl, L"AOIStride", &stride);
	validate(i_retCode, "Unable to get Stride value, retcode: ");
	return (int) stride;
}

double AndorCamera::getFrameRate()
{
	return frame_rate;
}

void validate(int i_retCode, std::string note)
// Prints out custom string if SDK return code is unsuccesful.
{
	if(i_retCode != AT_SUCCESS)	std::cout << note << i_retCode << std::endl;
}
